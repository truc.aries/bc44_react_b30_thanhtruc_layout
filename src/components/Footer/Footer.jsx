import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="bg-light">
        <div className="container text-center py-5 text-dark ">
          <h4 className="fs-6 fw-normal">Copyright &copy; Your Website 2019</h4>
        </div>
      </div>
    );
  }
}
